import java.util.*;
import java.io.*;
public class SerializeHashMap {
	 
	public static void main(String[] args)
    {
        HashMap<String, String> foodType = new HashMap<>();
      
        foodType.put("Burger", "Fastfood");
        foodType.put("Cherries", "Fruit");
         
        
        try {
            FileOutputStream myFileOutStream
                = new FileOutputStream(
                    "/Users/Anant/Downloads/Java/newHashMap.txt");
  
            ObjectOutputStream myObjectOutStream
                = new ObjectOutputStream(myFileOutStream);
  
            myObjectOutStream.writeObject(foodType);
            System.out.printf("Serialized HashMap data is saved");
            // closing FileOutputStream and
            // ObjectOutputStream
            myObjectOutStream.close();
            myFileOutStream.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

}
