abstract class School{
    School(){
        System.out.println("Central Academy");
    }
    abstract void run();
    void Subject(){
        System.out.println("Math");
    }

}
class Student1 extends School
{
    public void run()
    {
        System.out.println("Student 1");
    }
}
class Student2 extends School
{
    public void run()
    {
        System.out.println("Student 2");
    }
}
public class AbstractDemo {
    public static void main(String args[])
    {
        School sd=new Student1();
        sd.run();
        School sd2=new Student2();
        sd2.run();
        sd.Subject();
    }
}
